import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Article} from "../interfaces/article";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  apiUrl: string = 'https://localhost:8000/api/articles';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Article[]>{
    return this.http.get<Article[]>(this.apiUrl+".json");
  }

  getOne(id: number): Observable<Article> {
    return this.http.get<Article>(this.apiUrl+"/"+id+".json")
  }

  add(article: Article): Observable<Article> {
    return this.http.post(this.apiUrl+".json", article);
  }

  remove(id: number): Observable<Article> {
    return this.http.delete(this.apiUrl+'/'+id+'.json');
  }

}
