import { Component, OnInit } from '@angular/core';
import {Article} from "../../interfaces/article";
import {ArticleService} from "../../services/article.service";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  articles: Article[] = [];
  isLoading: boolean = false;

  constructor(private articleService: ArticleService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.articleService.getAll().subscribe(data=> {
      console.log(data);
      this.articles = data;
      console.log(this.articles);
      this.isLoading = false;
    })
  }

  deleteArticle(id?:number): void {
    this.isLoading = true;
    if(id){
      this.articleService.remove(id).subscribe(data => {
        this.ngOnInit();
      })
    }

  }

}
