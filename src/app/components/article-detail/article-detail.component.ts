import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ArticleService} from "../../services/article.service";
import {Article} from "../../interfaces/article";

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  article?: Article
  isLoading?: boolean;

  constructor(private activatedRoute: ActivatedRoute,
              private articleService: ArticleService) { }

  ngOnInit(): void {
    this.isLoading = true;
    let id = parseInt(<string>this.activatedRoute.snapshot.paramMap.get("id"));

    this.articleService.getOne(id).subscribe(data => {
      this.article = data;
      this.isLoading = false;
    })
  }

}
