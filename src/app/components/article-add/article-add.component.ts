import { Component, OnInit } from '@angular/core';
import {Article} from "../../interfaces/article";
import {ArticleService} from "../../services/article.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-article-add',
  templateUrl: './article-add.component.html',
  styleUrls: ['./article-add.component.css']
})
export class ArticleAddComponent implements OnInit {

  article: Article = new Article();
  isLoading = false;

  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit(): void {
  }

  submitForm(){
    this.isLoading = true;

    this.articleService.add(this.article).subscribe(data => {
      this.router.navigate(["/articles"]);
      this.isLoading = false;
    })

    console.log(this.article);
  }

}
