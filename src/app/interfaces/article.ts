export class Article {
  id?: number;
  titre?: string;
  image?: string;
  description?: string;


  constructor(id?: number, nom?: string, image?: string, description?: string) {
    this.id = id;
    this.titre = nom;
    this.image = image;
    this.description = description;
  }
}
