import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ArticleComponent} from "./components/article/article.component";
import {ArticleDetailComponent} from "./components/article-detail/article-detail.component";
import {ArticleAddComponent} from "./components/article-add/article-add.component";

const routes: Routes = [
  {path: 'articles', component: ArticleComponent},
  {path: 'articles/add', component: ArticleAddComponent},
  {path: 'articles/:id', component: ArticleDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
